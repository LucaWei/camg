
#ifndef __AMG_H__
#define	__AMG_H__

#include <iostream> 
#include <fstream>
#include <vector>
#include <algorithm>
#include <map>
#include <string> 
#include <cmath>

using std::vector;
using std::map;
using std::pair;
using std::string;
using std::ifstream;
using std::cout;
using std::endl;
using std::make_pair;

struct sparseMatrix
{
    map<pair<int,int>,double> mat; // value of sparse matrix
    map<pair<int,int>,int>    edge_list; // set neighbors 1
    map<pair<int,int>,int>    swd; // strong/weak dependency

   //-----------------------------------------

    int rows; // row    size
    int cols; // column size
    int nnz;  // number of non-zero elements

   //-----------------------------------------


    vector<int>         rows_val; // rows index of sparse matrix
    vector<int>         cols_val; // cols  index of sparse matrix
    vector<double>      value; // value of sparse matrix each rows, cols index
    vector<double>      b; // value of B matrix
    vector<double>      x; 
    vector<double>      xnew;
    vector<double>      e;
    vector<double>      r;
    vector<double>      eC;
    vector<double>      rC;

    vector<int> x_degree;
    vector<int> cg_point;
    vector<int> nnzPerRow;
    vector<int> cg_point_id;


    vector<int> ip_id;
    vector<double> ip_coef;
    int ip_cnt;

};

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 // Setup phase
 //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void AMG_set_data_level_0();
void AMG_set_data_level_n(int iLevel);
void CAMG_GenerateCoarseGrid(int iLevel);
void AMG_GetStrongDependencyMat(int iLevel, const double theta);
bool CAMG_InterpolateForPoint(int & iLevel, int irow);
sparseMatrix SparseMatrixMuliplication(sparseMatrix A, sparseMatrix B, const int &flag);
void CAMG_GenerateCoarseProblem ();



 //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 // Solver phase
 //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
vector<double> coo_spmv(vector<double> &A_coo_val, vector<int> &A_coo_row, vector<int>&A_coo_col, vector<double>&B_coo_val);
double product( vector<double> &a, vector<double> &b);
vector<double> BiCGSTAB(double zeta, int itmax, int flag, vector<double> &x0,vector<double> &A_coo_val, vector<int>&A_coo_row, vector<int>&A_coo_col, vector<double> &B_coo_val);
void AMG_smoother_SpMV( int& level , int& size , double *v , double *Av );
double AMG_smoother_calcInnerProduct( int& size , double *v1 , double *v2 ); 
void Coarses_CAMG_CG(int clevel);
void AMG_Solve_RestrictResidual(int iLevel);
void AMG_Solve_InterpolateError(int iLevel);
void Multigrid_Vcycle();



typedef sparseMatrix AmgData;

vector<AmgData> MatA;
vector<AmgData> MatC;
vector<AmgData> MatP;
vector<AmgData> MatR;


int iLevel = 0;
int nlevel = 2; // number of MatA level u need
int clevel = nlevel -1;


int row_, col_, nnz_;

vector<int> rr;
vector<int> cc;
vector<double> aa;
vector<double> bb;


 vector<double> xnew;
 vector<double> xold;







#endif
