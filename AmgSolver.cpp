

#include "AmgSolver.hpp"
#include "csr_matrix.hpp"
#include <chrono>
#include <stdexcept>
#include <iomanip>

using MatOps::csr_matrix;

//                    Multigrid_Solver
//                   /               \
// CAMG_GenerateCoarseProblem        Multigrid_Vcycle
//          |                               |
// CAMG_GenerateCoarseGrid              smoother
// CAMG_GetStrongDependencyMat
// CAMG_InterpolateForPoint



void AMG_set_data_level_0
    (
    // ======================================================== //
    
    // ======================================================== //
    )
{
    // ======================================================== //
    int row;
    // ======================================================== //
    
    // ======================================================== // Define 0 level MatA
    // define 0 level rows, cols ,nnz
    MatA[0].rows = row_;
    MatA[0].cols = col_;
    MatA[0].nnz  = nnz_;

    // for AMG_GenerateCoarseGrid
    MatA[0].x_degree.resize(MatA[0].rows);
    MatA[0].cg_point.resize(MatA[0].rows);
    MatA[0].nnzPerRow.resize(MatA[0].rows);

    // for CAMG_GenerateCoarseProblem
    MatA[0].cg_point_id.resize(MatA[0].rows);

    // for CAMG_InterpolateForPoint
    MatA[0].ip_coef.resize(MatA[0].rows);
    MatA[0].ip_id.resize(MatA[0].rows);

    for (int i = 0 ; i < MatA[0].rows ; ++i ) 
    { 
        MatA[0].nnzPerRow[i] = 0;
    }
    // ======================================================== //

    // ======================================================== // Input matrix data to 0 level MatA 
    MatA[0].rows_val.clear();
    MatA[0].cols_val.clear();
    MatA[0].value.clear();

    for(int i=0; i<MatA[0].nnz; ++i)
    {
        MatA[0].mat[ make_pair( rr[i]-1 , cc[i]-1 ) ] = aa[i];

        MatA[0].rows_val.push_back(rr[i]-1 );
        MatA[0].cols_val.push_back(cc[i]-1 );
        MatA[0].value.push_back(aa[i]);
    }

    for(int i=0; i<MatA[0].rows; ++i)
    {   
        MatA[0].b.push_back(bb[i]);
    }

    for (int i = 0 ; i < MatA[0].nnz ; ++i ) 
    { 
        row = MatA[0].rows_val[i];
        MatA[0].nnzPerRow[row]++;
    }
    // ======================================================== //
    

}


// later 
void AMG_set_data_level_n
    (
    // ======================================================== //
    int iLevel
    // ======================================================== //
    )
{
    // ======================================================== //
    int row;
    AmgData temp;
    // ======================================================== //

    // ======================================================== // Define next level MatA
    // define next level rows, cols ,nnz
    MatA[iLevel].rows = MatC[iLevel-1].rows;
    MatA[iLevel].cols = MatC[iLevel-1].cols;
    MatA[iLevel].nnz  = MatC[iLevel-1].nnz;

    // for AMG_GenerateCoarseGrid
    MatA[iLevel].x_degree.resize(MatC[iLevel-1].rows);
    MatA[iLevel].cg_point.resize(MatC[iLevel-1].rows);
    MatA[iLevel].nnzPerRow.resize(MatC[iLevel-1].rows);

    // for CAMG_GenerateCoarseProblem
    MatA[iLevel].cg_point_id.resize(MatC[iLevel-1].rows);

    // for CAMG_InterpolateForPoint
    MatA[iLevel].ip_coef.resize(MatC[iLevel-1].rows);
    MatA[iLevel].ip_id.resize(MatC[iLevel-1].rows);

    for (int i = 0 ; i < MatA[iLevel].rows ; ++i ) 
    { 
        MatA[iLevel].nnzPerRow[i] = 0;
    }
    // ======================================================== //


    // ======================================================== // MatC[n-1] into MatA[n]
    MatA[iLevel].cols_val.clear();
    MatA[iLevel].rows_val.clear();
    MatA[iLevel].value.clear();

    for(int i=0; i<MatC[iLevel-1].nnz; ++i)
    {
        MatA[iLevel].cols_val.push_back( MatC[iLevel-1].cols_val[i] );
        MatA[iLevel].rows_val.push_back( MatC[iLevel-1].rows_val[i] );
        MatA[iLevel].value.push_back( MatC[iLevel-1].value[i] );
        MatA[iLevel].mat[make_pair( MatA[iLevel].rows_val[i],MatA[iLevel].cols_val[i] )] = MatC[iLevel-1].value[i];
    }

    for (int i = 0 ; i < MatA[iLevel].nnz ; ++i ) 
    { 
        row = MatA[iLevel].rows_val[i];
        MatA[iLevel].nnzPerRow[row]++;
    }
    // ======================================================== //

 
}




void CAMG_GenerateCoarseGrid
    (
    // ======================================================== //
    int iLevel
    // ======================================================== //
    )
{
    // ======================================================== // Local variables
    int unvisited_points = 0;
    int new_cg_point = 0;
    int max_x_degree = 0;
    int neighbors_size = 0;
    int new_neighbors_size = 0;
    int curr_neighbor = 0;
    int isize = 0;
    int isize2 = 0;
    vector<int> nn;
    vector<int> nn2;
    vector<int> neighbors;
    vector<int> new_neighbors;

    
    // ======================================================== //


    //==================================================== Define edge_list
    for(int i=0; i<MatA[iLevel].nnz; i++)
    {
        if(MatA[iLevel].rows_val[i] != MatA[iLevel].cols_val[i])
            MatA[iLevel].edge_list[ make_pair( MatA[iLevel].rows_val[i], MatA[iLevel].cols_val[i] ) ] = 1;
    }
    //====================================================

    //==================================================== Define x_degree
    
    for(int i=0; i<MatA[iLevel].nnz; i++)
    {   
        MatA[iLevel].x_degree[ MatA[iLevel].rows_val[i] ]   +=  \
            MatA[iLevel].edge_list[ make_pair( MatA[iLevel].rows_val[i], MatA[iLevel].cols_val[i] ) ] * 1;
    }
    //====================================================
   
   
    //==================================================== Define cg_point and unvisited_points
    for(int i=0; i<MatA[iLevel].rows; i++)
    {
        MatA[iLevel].cg_point[i] = -1;
        unvisited_points++;
    }
    //====================================================





    while( unvisited_points>0 )
    {
        
        //==================================================== Define max_x_degree_idx
        max_x_degree = MatA[iLevel].x_degree[0];
        for(int i=0; i<MatA[iLevel].rows; i++)
        {
            if( MatA[iLevel].x_degree[i] > max_x_degree )
            {
                max_x_degree = MatA[iLevel].x_degree[i];
            }
        }

        for(int i=0; i<MatA[iLevel].rows; i++)
        {
            if( max_x_degree == MatA[iLevel].x_degree[i] )
            {
                new_cg_point = i;
                break;
            }
        }
        //====================================================



        //==================================================== Make cg_point to 1, x_degree to 0 at new_cg_point
        MatA[iLevel].cg_point[new_cg_point] = 1;
        MatA[iLevel].x_degree[new_cg_point] = 0;
        //====================================================


        //====================================================  Find new fine grid point as coarse point's neighbors
 

        neighbors_size = MatA[iLevel].nnzPerRow[new_cg_point] - 1;

        neighbors.resize( neighbors_size );
        neighbors.clear();

        for(int i=0; i<MatA[iLevel].cols; i++)
        {
            if( MatA[iLevel].edge_list[ make_pair(new_cg_point,i) ] == 1 )
                neighbors.push_back(i);
        }


        new_neighbors.resize(MatA[iLevel].rows);
        new_neighbors.clear();

        new_neighbors_size = 0;

        //neighbors 1,2
        for(int i=0; i<neighbors_size; i++)
        {

            if( MatA[iLevel].cg_point[ neighbors[i] ] == -1 )
            {
                new_neighbors.push_back(neighbors[i]);
                new_neighbors_size++;

                // Mark new neighbors
                MatA[iLevel].cg_point[neighbors[i]] = 0; //1,0,0,-1
                MatA[iLevel].x_degree[neighbors[i]] = 0; //0,0,0,2
            }
                
        }
        //==================================================== 


        //==================================================== Mark new neighbors and add their unvisited neighbors' weight
        for(int i=0; i<new_neighbors_size; i++)
        {
            curr_neighbor = new_neighbors[i];

            isize = MatA[iLevel].nnzPerRow[curr_neighbor] - 1;
            nn.resize( isize );
            nn.clear();


            for(int i=0; i<MatA[iLevel].cols; i++)
            {
                if( MatA[iLevel].edge_list[ make_pair(curr_neighbor,i) ] == 1 )
                    nn.push_back( i );
                
            }

            

            nn2.resize( isize );
            nn2.clear();

            isize2 = 0;

            for(int i=0; i<nn.size(); i++)
            {
                if( MatA[iLevel].cg_point[nn[i]] == -1 )
                {
                    nn2.push_back(nn[i]);
                    isize2++;
                }
            }

   

            for(int i=0; i<isize2; i++)
            {
                MatA[iLevel].x_degree[nn2[i]]++;
            }

        }
        //====================================================
        
        unvisited_points = 0;
        for(int i=0; i<MatA[iLevel].rows; i++)
        {
            if(MatA[iLevel].cg_point[i] == -1 )
                unvisited_points++;
        }
        
    }



    // for(int i=0; i<MatA[iLevel].rows; i++)
    //     cout <<  MatA[iLevel].cg_point[i] << endl;

    // for(int i=0; i<MatA[iLevel].rows; i++)
    //     cout <<  MatA[iLevel].x_degree[i] << endl;


}




void AMG_GetStrongDependencyMat
    (
    // ======================================================== //
    int iLevel,
    const double theta
    // ======================================================== //
    )
{
    // ======================================================== // Local variables
    vector<double> max;
    vector<double> nnz_in_row;
    vector<double> sd_in_row;
    vector<double> wd_in_row;
    double threshold;
    // ======================================================== //



    // ======================================================== // Find which neighbors is strong, which one is weak denpendency
    for(int i=0; i<MatA[iLevel].rows; ++i) //0,1,2,3
    {
        max.resize( MatA[iLevel].rows, 0.0 );
        max.clear();

        nnz_in_row.resize(MatA[iLevel].nnzPerRow[i]-1);
        nnz_in_row.clear();


        for(int j=0; j<MatA[iLevel].cols; ++j)
        {
            
            // cout << MatA[iLevel].edge_list[ make_pair(i,j) ] << endl;


            if( max[i] < -MatA[iLevel].mat[ make_pair(i,j) ] )
                max[i] = -MatA[iLevel].mat[ make_pair(i,j) ];

            if( MatA[iLevel].edge_list[ make_pair(i,j) ] ==1 )
                nnz_in_row.push_back(j);

        }

        threshold = theta * max[i];

        if ( threshold == 0 ) continue;


       

        for(int j=0; j<MatA[iLevel].cols; ++j)
        {
            if( i != j )    
                if(  -MatA[iLevel].mat[ make_pair(i,j) ] > threshold  )
                    MatA[iLevel].swd[ make_pair(i,j) ] = 1;
                else
                    MatA[iLevel].swd[ make_pair(i,j) ] = -1;

        }

    }
    // ======================================================== //

    // for(int i=0; i<MatA[iLevel].nnz; ++i)
    // {
    //     cout << MatA[iLevel].rows_val[i] << "  " \
    //          << MatA[iLevel].cols_val[i] << "  " \
    //          << MatA[iLevel].swd[ make_pair(MatA[iLevel].rows_val[i],MatA[iLevel].cols_val[i]) ] << endl;
    // }


}



bool CAMG_InterpolateForPoint
    (
    // ======================================================== //
    int & iLevel,
    int irow
    // ======================================================== //
    )
{
    // ======================================================== // Local variables
    vector<int> neighbors;
    vector<int> cg_neighbor_list;
    int neighbors_cnt = 0;
    int curr_neighbor = 0;
    int curr_check_neighbor = 0;
    int j = 0;
    double cg_neighbor_weight_sum = 0.0;
    double a_ii_new = 0.0;
    double contrib=0.0;
    int ip_id_pos = 0;
    // ========================================================
    
    // ======================================================== //  Init ip_id, ip_coef, cg_neighbor_list
    for(int i=0; i<MatA[iLevel].rows; ++i)
    {
        MatA[iLevel].ip_id[i]  = 0;
        MatA[iLevel].ip_coef[i] = 0.0;
    }

    cg_neighbor_list.clear();
    // ======================================================== //




    // ======================================================== //  If current point is coarse grid point, need not to interpolate
    MatA[iLevel].ip_cnt = 0;
    if( MatA[iLevel].cg_point[irow]==1 )
    {
        MatA[iLevel].ip_id[0] = irow;
        MatA[iLevel].ip_coef[0] = 1.0;
        MatA[iLevel].ip_cnt++;
        return true;
    }
    // ======================================================== //




    // ======================================================== // Find all coarse grid points for interpolation
    neighbors.resize(MatA[iLevel].nnzPerRow[irow]-1);
    neighbors_cnt = 0;
    for(int i=0; i<MatA[iLevel].cols; ++i)
    {
        if(i != irow && MatA[iLevel].mat[make_pair(irow,i)]!=0)
        {
            neighbors[neighbors_cnt] = i;
            neighbors_cnt++;   
        }
    }

    MatA[iLevel].ip_cnt = 0;

    for(int i=0; i<neighbors_cnt; ++i)
    {
        curr_neighbor = neighbors[i];
        


        if( MatA[iLevel].cg_point[curr_neighbor]==1 ) // cg_point = 1,0,0,1
        {
            
            MatA[iLevel].ip_id[MatA[iLevel].ip_cnt] = curr_neighbor;

            MatA[iLevel].ip_coef[MatA[iLevel].ip_cnt] = MatA[iLevel].mat[make_pair(irow, curr_neighbor)];

            MatA[iLevel].ip_cnt++;
        }
    }
    // ======================================================== //


    
    // ======================================================== // Redistribute the edge weight of all fine grid points with weak dependency
    a_ii_new = MatA[iLevel].mat[make_pair(irow, irow)];
    for(int i=0; i<neighbors_cnt; ++i)
    {
        curr_neighbor = neighbors[i]; //curr_neighbor = 0, 3
        // cg_point = 1,0,0,1
        if(     MatA[iLevel].cg_point[curr_neighbor]==0 &&  MatA[iLevel].swd[make_pair(irow, curr_neighbor)]==-1    )
        {
            a_ii_new += MatA[iLevel].mat[make_pair(irow, curr_neighbor)];
        }

    }
    // ======================================================== //


    // ======================================================== // Redistribute the edge weight of all fine grid points with strong dependency
    for(int i=0; i<neighbors_cnt; ++i)
    {
        curr_neighbor = neighbors[i]; //curr_neighbor = 0, 3
        // cg_point = 1,0,0,1
        if(     MatA[iLevel].cg_point[curr_neighbor]==0 &&  MatA[iLevel].swd[make_pair(irow, curr_neighbor)]==1    )
        {
            cg_neighbor_list.clear();
            cg_neighbor_weight_sum = 0;

            // Find all coarse grid points connected to curr_neighbor
            // and sum the edge weight of these connections
            for(int j=0; j<neighbors_cnt; ++j)
            {
                curr_check_neighbor = neighbors[j]; //i_point's neighbors k_point  1st: 0; 2nd: 3
                if(     MatA[iLevel].cg_point[curr_check_neighbor]==1 &&  MatA[iLevel].mat[make_pair(curr_neighbor, curr_check_neighbor)]!=0.0    )
                {
                    cg_neighbor_weight_sum += MatA[iLevel].mat[make_pair(curr_neighbor, curr_check_neighbor)]; //delta_k
                    cg_neighbor_list.push_back(cg_neighbor_weight_sum);
                }
            }

            // Redistribute the edge weight A(curr_pid, curr_neighbor) to each
            // neighboring coarse grid poin
            for(int k=0; k<cg_neighbor_list.size(); ++k)
            {
                j = cg_neighbor_list[k];
                curr_check_neighbor = neighbors[j];
                // A(2,1)/delta_k * A(2,4)
                contrib = MatA[iLevel].mat[make_pair(curr_neighbor,curr_check_neighbor)] * \
                          MatA[iLevel].mat[make_pair(irow,curr_neighbor)] / \
                          cg_neighbor_weight_sum;


                for(int i=0; i< MatA[iLevel].ip_cnt; ++i)
                {
                    if(MatA[iLevel].ip_id[i]==curr_check_neighbor)
                    {
                        ip_id_pos = i;
                        break;
                    }
                }
                MatA[iLevel].ip_coef[ip_id_pos] += contrib;
            } // k end

            

        } // if end


    } // i end
    // ======================================================== //

    for(int i=0; i<MatA[iLevel].ip_cnt; ++i)
    {
        MatA[iLevel].ip_coef[i] /= -a_ii_new;
    }



    return true;
}



sparseMatrix SparseMatrixMuliplication
    (
    // ======================================================== //
    sparseMatrix A,
    sparseMatrix B,
    const int &flag
    // ======================================================== //
    )
{
    // ======================================================== // Local variables
    sparseMatrix output;
    // ======================================================== //

    // ======================================================== // Define  output matrix rows, cols, vlaue, nnz
    if(A.cols != B.rows)
    {
        throw std::invalid_argument("matrix size doesn't match!");
    }
    else
    {
        output.rows = A.rows;
        output.cols = B.cols;
    }

    if(flag==1)
    {
        output.nnz = 0;
        output.rows_val.clear();
        output.cols_val.clear();
        output.value.clear();
    }
    // ======================================================== //


    // ======================================================== // Start Muliplication
    for(auto i=A.mat.begin(); i!=A.mat.end(); ++i)
    {
        if((*i).first.first>A.rows-1 || (*i).first.second>A.cols-1)
            throw std::invalid_argument("A index out of bound");

        for(auto j=B.mat.begin(); j!=B.mat.end(); ++j)
        {
            if((*j).first.first>B.rows-1)
                throw std::invalid_argument("B index out of bound");
            if((*j).first.second>B.cols-1)
                throw std::invalid_argument("cols B index out of bound");
            



            // ======================================================== //
            if( (*i).first.second == (*j).first.first ) // cols of A matrix = rows of B matrix 
            {
                if(output.mat.find(  make_pair( (*i).first.first,(*j).first.second )  ) == output.mat.end()   )
                {
                    output.mat[ make_pair((*i).first.first,(*j).first.second) ] = (*i).second * (*j).second;
                    output.rows_val.push_back((*i).first.first);
                    output.cols_val.push_back((*j).first.second);
                    output.nnz++;
                }
                else
                {
                    output.mat[ make_pair((*i).first.first,(*j).first.second) ] += (*i).second * (*j).second;
                }
            }
            // ======================================================== //




            
        } //j end
    } // i end
    // ======================================================== //

    if(flag==1)
    {
        for(int i=0; i<output.nnz; ++i)
        {
            output.value.push_back( output.mat[make_pair(output.rows_val[i], output.cols_val[i])] );
        }
    }


    return output;
}











void CAMG_GenerateCoarseProblem 
    (
    // ======================================================== //
    
    // ======================================================== //
    )
{
    // ======================================================== // Local variables
    int CA_n = 0;
    int p_row = 0;
    int p_col = 0;
    // ======================================================== //


    

    // ======================================================== //
    AMG_set_data_level_0(); // Set level = 0 for info of matrix
    // ======================================================== //

    for(iLevel=0; iLevel <nlevel-1; ++iLevel)   // nlevel = 2
    {
        // ======================================================== //
        if(iLevel>0) AMG_set_data_level_n(iLevel); // Set level = n for info of matrix
        // ======================================================== //
        
        // ======================================================== //
        CAMG_GenerateCoarseGrid(iLevel); // generate the coarse grid
        // ======================================================== //


        // ======================================================== // AMG cumsum
        for(int i=0; i<MatA[iLevel].rows;++i)
        {
            MatA[iLevel].cg_point_id[i] = 0;
        }
        CA_n = 0;
        for(int i=0; i<MatA[iLevel].rows;++i)
        {
            CA_n += MatA[iLevel].cg_point[i];
            MatA[iLevel].cg_point_id[i] = CA_n;
        }
        // ======================================================== //

        // ======================================================== //
        AMG_GetStrongDependencyMat(iLevel, 0.5); // generate strong/weak depedency
        // ======================================================== //



        // ======================================================== // init MatP, MatR, MatC
        MatP[iLevel].rows_val.clear();
        MatP[iLevel].cols_val.clear();
        MatP[iLevel].value.clear();

        MatR[iLevel].rows_val.clear();
        MatR[iLevel].cols_val.clear();
        MatR[iLevel].value.clear();

        MatP[iLevel].rows = MatA[iLevel].rows;
        MatP[iLevel].cols = MatA[iLevel].cg_point_id[MatA[iLevel].rows-1];
        MatP[iLevel].nnz  = 0;

        MatR[iLevel].rows = MatP[iLevel].cols; 
        MatR[iLevel].cols = MatP[iLevel].rows;
        MatR[iLevel].nnz  = 0;

        MatC[iLevel].rows = MatA[iLevel].cg_point_id[MatA[iLevel].rows-1];
        MatC[iLevel].cols = MatA[iLevel].cg_point_id[MatA[iLevel].rows-1];
        MatC[iLevel].nnz  = 0;

        
        
        // ======================================================== //

    

        // ======================================================== // Build interpolation, restriction operator matrix
        for(int irow=0; irow<MatA[iLevel].rows; ++irow)
        {

            CAMG_InterpolateForPoint(iLevel,irow); 

            // Fill in the interpolation matrix
            for(int i=0; i<MatA[iLevel].ip_cnt; ++i)  // ip_cnt = 2
            {

                // ======================================================== // Build interpolation operator matrix
                p_row = irow;
                p_col = MatA[iLevel].cg_point_id[MatA[iLevel].ip_id[i]] - 1;

                MatP[iLevel].mat[make_pair(p_row, p_col)] = MatA[iLevel].ip_coef[i];
                MatP[iLevel].rows_val.push_back(p_row);
                MatP[iLevel].cols_val.push_back(p_col);
                MatP[iLevel].value.push_back(MatA[iLevel].ip_coef[i]);

                MatP[iLevel].nnz++;
                // ======================================================== //

                // ======================================================== // Build restriction operator matrix
                MatR[iLevel].mat[make_pair(p_col, p_row)] = MatA[iLevel].ip_coef[i];
                MatR[iLevel].rows_val.push_back(p_col);
                MatR[iLevel].cols_val.push_back(p_row);
                MatR[iLevel].value.push_back(MatA[iLevel].ip_coef[i]);

                
                MatR[iLevel].nnz++;
                // ======================================================== // 


            } // i end

        }// irow end
        // ======================================================== //



        // ======================================================== // Using new Luca's container
        csr_matrix<double> tmpMatA(MatA[iLevel].rows, MatA[iLevel].cols);
        csr_matrix<double> tmpMatP(MatP[iLevel].rows, MatP[iLevel].cols);
        csr_matrix<double> tmpMatR(MatR[iLevel].rows, MatR[iLevel].cols);
        
        for(int i=0; i<MatA[iLevel].nnz; ++i)
        {
            tmpMatA.set(MatA[iLevel].value[i], MatA[iLevel].rows_val[i], MatA[iLevel].cols_val[i]);
        }

        for(int i=0; i<MatP[iLevel].nnz; ++i)
        {
            tmpMatP.set(MatP[iLevel].value[i], MatP[iLevel].rows_val[i], MatP[iLevel].cols_val[i]);
        }

        for(int i=0; i<MatR[iLevel].nnz; ++i)
        {
            tmpMatR.set(MatR[iLevel].value[i], MatR[iLevel].rows_val[i], MatR[iLevel].cols_val[i]);

            
        }
        // ======================================================== //

     
        // ======================================================== // Construct coarse grid matrix. CA = R' * A * P
        csr_matrix<double> tmpMat1(MatR[iLevel].rows, MatA[iLevel].cols);
        csr_matrix<double> tmpMat2(MatR[iLevel].rows, MatP[iLevel].cols);

        tmpMat1 = tmpMatR * tmpMatA;
        tmpMat2 = tmpMat1 * tmpMatP;
    
        // ======================================================== // 


        // ======================================================== // Define MatC
        int *coo_row = new int[tmpMat2.getNnzCount()];
        int a = 0;
        for(int i=0; i<tmpMat2.getRowCount(); ++i)
        {
            for(int j= tmpMat2.getCsrRowList(i); j<tmpMat2.getCsrRowList(i+1); ++j)
            {
                coo_row[j] = a;
            }
                a++;
        }
        
        MatC[iLevel].cols_val.clear();
        MatC[iLevel].rows_val.clear();
        MatC[iLevel].value.clear();
        MatC[iLevel].nnz = 0;
        
        for(int i=0; i<tmpMat2.getNnzCount(); ++i)
        {
            MatC[iLevel].cols_val.push_back(tmpMat2.getColumnList(i));
            MatC[iLevel].rows_val.push_back(coo_row[i]);
            MatC[iLevel].value.push_back(tmpMat2.getValueList(i));

            MatC[iLevel].mat[make_pair(coo_row[i] , tmpMat2.getColumnList(i))] = tmpMat2.getValueList(i);
            MatC[iLevel].nnz++;
        }
        delete [] coo_row;  
        // ======================================================== // 





        

        // // ======================================================== // Construct coarse grid matrix. CA = R' * A * P
        // sparseMatrix tmpMat1;
        // sparseMatrix tmpMat2;
        
        // tmpMat1.rows = MatR[iLevel].rows;
        // tmpMat1.cols = MatA[iLevel].cols;

        // tmpMat1 = SparseMatrixMuliplication(MatR[iLevel], MatA[iLevel], 0);

        // tmpMat2.rows = tmpMat1.rows;
        // tmpMat2.cols = MatP[iLevel].cols;

        // tmpMat2 = SparseMatrixMuliplication(tmpMat1, MatP[iLevel], 1); // CA
        // // ======================================================== // 
        


        // // ======================================================== // Define MatC
        // MatC[iLevel].cols_val.clear();
        // MatC[iLevel].rows_val.clear();
        // MatC[iLevel].value.clear();
        // MatC[iLevel].nnz =0;
        
        // for(int i=0; i<tmpMat2.nnz; ++i)
        // {
        //     MatC[iLevel].cols_val.push_back(tmpMat2.cols_val[i]);
        //     MatC[iLevel].rows_val.push_back(tmpMat2.rows_val[i]);
        //     MatC[iLevel].value.push_back(tmpMat2.value[i]);

        //     MatC[iLevel].mat[make_pair(tmpMat2.rows_val[i], tmpMat2.cols_val[i])] = tmpMat2.value[i];
        //     MatC[iLevel].nnz++;
        // }
        // // ======================================================== // 




        // ======================================================== // Show the coarsing information
        double ratio = ( MatC[iLevel].nnz * 1.0 / MatA[0].nnz * 1.0 ) * 100.0;
        cout << "  >>> Level = " << iLevel+2 << " , matrix size = " << std::setw(3) 
            << MatA[iLevel].cg_point_id[MatA[iLevel].rows-1] 
            << " , non-zero percentage = " << ratio << endl;
        // ======================================================== // 
        

        
    } // iLevel-loop end
    AMG_set_data_level_n(nlevel-1);

    

}





vector<double> coo_spmv
    (
    // ======================================================== //

    vector<double> &A_coo_val,  
    vector<int>    &A_coo_row,  
    vector<int>    &A_coo_col,  

    vector<double> &B_coo_val

    // ======================================================== //
    )
{
    // ======================================================== //

    vector<double>       y (B_coo_val.size());
    
    // ======================================================== //

    

    for(int i=0; i<A_coo_val.size(); ++i)
    {
        y[A_coo_row[i]] +=  A_coo_val[i] * B_coo_val[A_coo_col[i]];    
    }

    return y;

}



double product
    (
    // ======================================================== //
    vector<double> &a,
    vector<double> &b
   

    // ======================================================== //
    )
{
    // ======================================================== //
    double c = 0.0;
    
    // ======================================================== //

    
    for(int i=0; i<a.size();++i)
    {
        c += a[i] * b[i];
    }

    return c;
}


vector<double> BiCGSTAB
    (
    // ======================================================== //
    double zeta, int itmax, int flag,

    vector<double> &x0,

    vector<double> &A_coo_val,  
    vector<int>    &A_coo_row,  
    vector<int>    &A_coo_col,  

    vector<double> &B_coo_val


    // ======================================================== //
    
    )
{
    
    // ======================================================== //

    double alpha,beta,nu,mu,norm0,norm,sum,scal,norm1,norm2,omega,rho1,rho2;

    int ik, icel;

    const int caliceltot = B_coo_val.size();
    
    vector<double> p(caliceltot);
    
    vector<double> x(caliceltot);
	
    vector<double> r(caliceltot);

    vector<double> r2(caliceltot);

    vector<double> v(caliceltot);

    vector<double> ss(caliceltot);

    vector<double> t(caliceltot);


    // ======================================================== //


    if(flag ==0)
    {
        for ( int i = 0; i < caliceltot; i++ ) x[i] = MatA[iLevel].x[i];     
    }
    if(flag ==1)
    {
        for ( int i = 0; i < caliceltot; i++ ) x[i] = MatA[iLevel].x[i] + MatA[iLevel].e[i];     
    }

    

    norm0 = product(B_coo_val, B_coo_val);

    norm0 = sqrt(norm0); 

    p = coo_spmv(A_coo_val, A_coo_row, A_coo_col, x);

    for(int i=0; i<caliceltot; ++i)
        r[i] = B_coo_val[i] - p[i];

    for(int i=0; i<caliceltot; ++i)
        r2[i] = r[i];

    rho1  = 1;
    alpha = 1;
    omega = 1;

    for(int i=0; i<caliceltot; ++i)
    {
        v[i] = 0;
        p[i] = 0;
    }

    norm = 0.0;

    for(int i=0; i<caliceltot; ++i)
        norm += r[i] * r[i];

    norm = sqrt(norm) / norm0;

    ik = 0;

    while(ik<itmax)
    {
        // for(int i=0; i<caliceltot; ++i)
        //     x[i] = x0[i];

        rho2 = product(r2, r);

        beta = (rho2/rho1) * (alpha/omega);

        for(int i=0; i<caliceltot; ++i)
            p[i] = r[i] + beta * (p[i] - omega * v[i]);

        v = coo_spmv(A_coo_val, A_coo_row, A_coo_col, p);

        alpha = rho2 / product(r2, v);

        for(int i=0; i<caliceltot; ++i)
            ss[i] = r[i] - alpha * v[i];

        t = coo_spmv(A_coo_val, A_coo_row, A_coo_col, ss);

        omega = product(t, ss) / product(t, t);

        for(int i=0; i<caliceltot; ++i)
            x[i] += alpha * p[i] + omega * ss[i];

        for(int i=0; i<caliceltot; ++i)
            r[i] = ss[i] - omega * t[i];

        rho1 = rho2;

        norm = 0.0;

        for(int i=0; i<caliceltot; ++i)
            norm += r[i] * r[i];

        norm = sqrt(norm) / norm0;
        
        ++ik;
    }



    return x;
}



void AMG_smoother_SpMV( int& level , int& size , double *v , double *Av ) 
{
   
   int row , col ;
   
   //----------------
   // Initialization
   //----------------
   
   for ( int i = 0 ; i < size ; i++ ) Av[i] = 0.0 ;
   
   for ( int innz = 0 ; innz < MatA[iLevel].nnz ; innz++ ) {
   	 row      = MatA[iLevel].rows_val[innz] ;
   	 col      = MatA[iLevel].cols_val[innz] ;
     Av[row] += MatA[iLevel].value[innz] * v[col] ;
}
}

double AMG_smoother_calcInnerProduct( int& size , double *v1 , double *v2 ) 
{
   double InnerProduct = 0.0 ;
   for ( int i = 0 ; i < size ; i++ ) InnerProduct += v1[i] * v2[i] ;   
   return InnerProduct ;
 }





void Coarses_CAMG_CG
    (
    // ======================================================== //
    int clevel
    // ======================================================== //
    )
{
    // ======================================================== // local variables
    int size;   
    double alpha, beta, rho1, rho2;
    double L2;
    int row_idx;
    int col_idx;
    size = MatA[clevel].rows;

    double *b   = new double[size] ;
    double *xk  = new double[size] ;
    double *rk  = new double[size] ;
    double *pk  = new double[size] ;
    double *Apk = new double[size] ;
    double *Axk = new double[size] ;
    // ======================================================== // 

    for ( int i = 0 ; i < size ; i++ ) 
    {
        b  [i] = MatA[clevel].b[i] ;   
        xk [i] = 1e-16 ;   
        Axk[i] = 0.0   ;
    }

   for ( int innz = 0 ; innz < MatA[clevel].nnz ; innz++ ) 
   {
        row_idx       = MatA[clevel].rows_val[innz] ;
        col_idx       = MatA[clevel].cols_val[innz] ;
        Axk[row_idx] += MatA[clevel].value[innz] * xk[col_idx] ;
   }

   for ( int i = 0 ; i < size ; i++ ) rk[i] = b[i] - Axk[i];

   rho1 = AMG_smoother_calcInnerProduct( size , rk , rk );

   int iter = 0;

    while(1)
    {
        iter++;
        if ( iter == 1 ) 
        {
            for ( int i = 0 ; i < size ; i++ ) pk[i] = rk[i]; 
        } 
        else 
        {	 
            beta = rho1 / rho2 ;
            for ( int i = 0 ; i < size ; i++ ) pk[i] = rk[i] + beta * pk[i];
        }

        for ( int i = 0 ; i < size ; i++ ) Apk[i] = 0.0 ;
   
        for ( int innz = 0 ; innz < MatA[clevel].nnz ; innz++ ) 
        {
            row_idx       = MatA[clevel].rows_val[innz];
            col_idx       = MatA[clevel].cols_val[innz];
            Apk[row_idx] += MatA[clevel].value[innz] * pk[col_idx];
        }

        alpha = rho1 / AMG_smoother_calcInnerProduct( size , pk , Apk );

        for ( int i = 0 ; i < size ; i++ ) xk[i] += alpha * pk[i];

        for ( int i = 0 ; i < size ; i++ ) rk[i] -= alpha * Apk[i];

        rho2 = rho1;

        rho1 = AMG_smoother_calcInnerProduct( size , rk , rk );

        L2 = 0.0;
        for ( int i = 0 ; i < size ; i++ ) L2 += rk[i] * rk[i];
	    L2 = sqrt( L2 / size );
        if(L2 < 1e-12)
        {
            MatC[clevel].eC.clear();   
       
            for ( int i = 0 ; i < size ; i++ ) MatC[clevel].eC.push_back( xk[i] );
            
            delete[]  b  ;
            delete[]  xk ; 
            delete[]  rk ; 
            delete[]  pk ; 
            delete[] Apk ;
            delete[] Axk ;	    
            break; 
        }
    }

}



void AMG_Solve_RestrictResidual
(
    // ======================================================== //
    int iLevel
    // ======================================================== //
)
{
    // ======================================================== // Local variables
    int row;
    int col;
    double *AXtmp = new double[MatA[iLevel].rows];
    double *RAtmp = new double[MatR[iLevel].rows];
    // ======================================================== //

    for(int i=0; i<MatA[iLevel].rows; ++i)
    {
        AXtmp[i] = 0.0;
    }


    for(int i=0; i<MatA[iLevel].nnz; ++i)
    {
        AXtmp[ MatA[iLevel].rows_val[i] ] +=  MatA[iLevel].value[i] * MatA[iLevel].x[ MatA[iLevel].cols_val[i] ];   
    }

    for(int i=0; i<MatA[iLevel].rows; ++i)
    {
        MatA[iLevel].r[i] = MatA[iLevel].b[i] - AXtmp[i];
    }


    for(int i=0; i<MatR[iLevel].rows; ++i)
    {
        RAtmp[i] = 0.0;
    }


    for(int j=0; j<MatR[iLevel].nnz; ++j)
    {
        row = MatR[iLevel].rows_val[j];
        col = MatR[iLevel].cols_val[j];

        RAtmp[row] += MatR[iLevel].value[j] * MatA[iLevel].r[col];
    }

    MatA[iLevel+1].b.resize(MatA[iLevel].rows);
    MatA[iLevel+1].b.clear();

    for ( int i = 0 ; i < MatR[iLevel].rows ; i++ ) 
    {
        MatA[iLevel+1].b.push_back( RAtmp[i] ) ;
    }

    delete[] AXtmp;
    delete[] RAtmp;

}

void AMG_Solve_InterpolateError
(
    // ======================================================== //
    int iLevel
    // ======================================================== //
)
{
    // ======================================================== // Local variables
    int row;
    int col;
    double *PeCtmp = new double[MatP[iLevel].rows];
    double *Xtmp;
    // ======================================================== //

    if(iLevel==clevel-1)
    {
        Xtmp = new double[MatA[clevel].rows];

        for(int i=0; i<MatA[clevel].rows; ++i)
            Xtmp[i] = MatC[clevel].eC[i];
    }
    else
    {
        Xtmp = new double[MatA[iLevel+1].rows];

        for(int i=0; i<MatA[iLevel+1].rows; ++i)
            Xtmp[i] = MatA[iLevel+1].x[i];
    }

    // Interpolate the error
    for(int i=0; i<MatP[iLevel].rows; ++i)
    {
        PeCtmp[i] = 0;
    }

    for(int j=0; j<MatP[iLevel].nnz; ++j)
    {
        row = MatP[iLevel].rows_val[j];
        col = MatP[iLevel].cols_val[j];

        PeCtmp[row] += MatP[iLevel].value[j] * Xtmp[col];
    }

    MatA[iLevel].e.clear();

    for(int i=0; i<MatA[iLevel].rows; ++i)
    {
        MatA[iLevel].e.push_back(PeCtmp[i]);
    }

  

}


void Multigrid_Vcycle
(
    // ======================================================== //
    
    // ======================================================== //
)
{
    // ======================================================== // Local variables
    int CAMGIters;
    int itmax = 1;
    double L2 = 0.0;
    double rn = 1.0;
    double rn_stop = 1.0e-5;
    double *AXtmp = new double[MatA[0].rows];
    double *r = new double[MatA[0].rows];
    // ======================================================== //
    

    for(int iLevel=0; iLevel<nlevel; ++iLevel)
    {
        MatA[iLevel].x.resize(MatA[iLevel].rows);
        MatA[iLevel].x.clear();
        MatA[iLevel].r.resize(MatA[iLevel].rows);
        MatA[iLevel].r.clear();
        for(int i=0; i<MatA[iLevel].rows; ++i)
        {
            MatA[iLevel].x.push_back(0.0);
            MatA[iLevel].r.push_back(0.0);
        }
    }
    xnew.resize(MatA[0].rows);
    xold.resize(MatA[0].rows);

    for(int i=0; i<MatA[0].rows; ++i)
    {
        xnew[i] = 0.0;
        xold[i] = 0.0;
    }
    // ======================================================== // AMG V-cycle iteration
    CAMGIters = 0;
    while(rn > rn_stop)
    {
        for(iLevel=0; iLevel < nlevel-1; ++iLevel)
        {
            // --- Step 1 : Pre-smooth -> get x0
            MatA[iLevel].x = BiCGSTAB(1e-10, itmax, 0, MatA[iLevel].x, \
            MatA[iLevel].value, MatA[iLevel].rows_val, MatA[iLevel].cols_val, \
            MatA[iLevel].b);

           

            // --- Step 2 : Restrict the residual -> find rC (Mat[iLevel+1].b)
            AMG_Solve_RestrictResidual(iLevel);
            

         
        }
      


        // // --- Step 3 : Solve the error equation in the coarest-grid level. find eC
        Coarses_CAMG_CG(clevel);
        


        for(iLevel=clevel-1; iLevel>=0; iLevel--)
        {
       
            // --- Step 4 : Interpolate the error from coarse grid to fine grid. find e
            AMG_Solve_InterpolateError(iLevel);
        
            // --- Step 5 : Pos-smooth
            MatA[iLevel].x = BiCGSTAB(1e-10, itmax, 1, MatA[iLevel].x, \
            MatA[iLevel].value, MatA[iLevel].rows_val, MatA[iLevel].cols_val, \
            MatA[iLevel].b);


        }
        


        // ======================================================== // cal CAMG Residual
       for(int i=0; i<MatA[0].nnz; ++i)
        {
            AXtmp[i] = 0.0;
        }

       for(int i=0; i<MatA[0].nnz; ++i)
        {
            AXtmp[ MatA[0].rows_val[i] ] +=  MatA[0].value[i] * MatA[0].x[ MatA[0].cols_val[i] ];   
        }

        for(int i=0; i<MatA[0].rows; ++i)
        {
            r[i] = MatA[0].b[i] - AXtmp[i];
        }

        rn = 0.0;

        for ( int i = 0 ; i < MatA[0].rows ; i++ )
            rn += r[i]*r[i] ; 

	    rn = sqrt( rn /MatA[0].rows );
	 
        ++CAMGIters;
        // ======================================================== //
        cout << "   >>> rn " << rn << endl;
       
  
    

    
    }
    // ======================================================== //
    cout << "   >>> Num Of Iter = " << CAMGIters << ", CAMG Residual = " << rn << endl;
    cout << endl;
    
    


}




// ======================================================== //
//                                                          //
//                      main program                        //
//                                                          //
// ======================================================== //

 int main()
 {
    // ======================================================== //
  
    
    

    // Problem 1 4x4 matrix
    string FileName1 = "A_matrix.mtx";
    string FileName2 = "B_matrix.mtx";

    // string FileName1 = "gr_30_30_mat.mtx";
    // string FileName2 = "gr_30_30_rhs.mtx";
    ifstream inputMatData(FileName1);
    ifstream inputRhsData(FileName2);

    MatA.resize(nlevel);
    MatP.resize(nlevel);
    MatR.resize(nlevel);
    MatC.resize(nlevel);
    // ======================================================== //




    inputMatData >> row_ >>col_ >> nnz_;

    rr.resize(nnz_);
    cc.resize(nnz_);
    aa.resize(nnz_);
    bb.resize(row_);
    
    //read "A_matrix.mtx"
    for(int i=0; i<nnz_; ++i)
    {
        inputMatData >> rr[i] >> cc[i] >> aa[i];
    }

    //read "B_matrix.mtx"
    for(int i=0; i<row_; ++i)
    {   
        inputRhsData >>  bb[i];
    }
    
//************************************************************************************
//************************************************************************************







 

    
//  CAMG Start !!!
//************************************************************************************
//************************************************************************************
    double RunTime_setup;
    double RunTime_solve;

    cout << endl;
    cout << " ================================ " << endl;
    cout << "           Setup phase            " << endl;
    cout << " ================================ " << endl;
    cout << endl;

    std::chrono::steady_clock::time_point ts_setup = std::chrono::steady_clock::now();

    // ======================================================== //
    CAMG_GenerateCoarseProblem();  // Run Setup phase  
    // ======================================================== //
  
    std::chrono::steady_clock::time_point te_setup = std::chrono::steady_clock::now();

    RunTime_setup += (std::chrono::duration_cast<std::chrono::microseconds>( te_setup - ts_setup ).count())/1000000.0;
    
    cout << endl;
    cout << " ================================ " << endl;
    cout << "            Solve phase          " << endl;
    cout << " ================================ " << endl;
    cout << endl;

    std::chrono::steady_clock::time_point ts_solve = std::chrono::steady_clock::now();

    // ======================================================== //
    Multigrid_Vcycle(); // AMG V-cycle iteration   
    // ======================================================== //

    std::chrono::steady_clock::time_point te_solve = std::chrono::steady_clock::now();

    RunTime_solve += (std::chrono::duration_cast<std::chrono::microseconds>( te_solve - ts_solve ).count())/1000000.0;

    cout << std::endl;
    cout << " ================================ " << std::endl;
    cout << "          Runtime                 " << std::endl;
    cout << " ================================ " << std::endl;
    cout << std::endl;
    cout << "  >>> Setup = " << RunTime_setup     << std::endl; 
    cout << "  >>> Solve = " << RunTime_solve     << std::endl; 
    cout << std::endl;

    cout << "Ans " << endl;
    cout.setf(std::ios_base::scientific);
    for(int j=0; j<MatA[0].rows; ++j)
    {
        cout << MatA[0].x[j] <<endl;
    }

    // 0.123119982955653
    // -0.107388888768714
    // 0.110724137102982
    // -0.119621804507259
    return 0;
 }
