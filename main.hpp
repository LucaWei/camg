 
#include <iostream> 
#include <fstream>
#include <vector>
#include <algorithm>
#include <map>
#include <string> 
#include <cmath>

using std::vector;
using std::map;
using std::pair;
using std::string;
using std::ifstream;
using std::cout;
using std::endl;
using std::make_pair;


//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 // Setup phase
 //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

extern void CAMG_GenerateCoarseProblem ();


 // Solver phase
 //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
extern vector<double> coo_spmv(vector<double> &A_coo_val, vector<int> &A_coo_row, vector<int>&A_coo_col, vector<double>&B_coo_val);
extern double product( vector<double> &a, vector<double> &b);
extern vector<double> BiCGSTAB(double zeta, int itmax, int flag, vector<double> &x0,vector<double> &A_coo_val, vector<int>&A_coo_row, vector<int>&A_coo_col, vector<double> &B_coo_val);
extern void AMG_smoother_SpMV( int& level , int& size , double *v , double *Av );
extern double AMG_smoother_calcInnerProduct( int& size , double *v1 , double *v2 ); 
extern void Coarses_CAMG_CG(int clevel);
extern void AMG_Solve_RestrictResidual(int iLevel);
extern void AMG_Solve_InterpolateError(int iLevel);
extern void Multigrid_Vcycle();
